# Pulumi Docker-In-Docker

This image extends the [pulumi/pulumi-python](https://hub.docker.com/r/pulumi/pulumi-python) image, provided by [Pulumi](https://www.pulumi.com). It adds Docker-in-Docker which allows the user to build/push Docker images using the [Pulumi_Docker](https://www.pulumi.com/registry/packages/docker) package.

Image can be found on [DockerHub](https://hub.docker.com/r/mattflowersilg/pulumi_docker-in-docker).
